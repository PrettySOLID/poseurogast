import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class JSON extends Export {

    public JSON(ArrayList<Artikel> list){
        super(list);
    }

    public void export(ArrayList<Artikel> list) {
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println("[");
        for(Artikel a : list){
            System.out.println("\t{");
            System.out.println("\t\t\"Nummer\":" + a.getNummer()+",");
            System.out.println("\t\t\"Bezeichnung\":" + a.getBezeichnung()+",");
            System.out.println("\t\t\"Einkaufspreis\":" + a.getEinkaufspreis()+",");
            System.out.println("\t\t\"Ablaufdatum\":" +date.format(a.getAblaufdatum().getTime())+",");
            System.out.println("\t\t\"Kategorienummer\":" + a.getKategorienummer()+",");
            System.out.println("\t},");
        }
        System.out.println("]");
    }
}