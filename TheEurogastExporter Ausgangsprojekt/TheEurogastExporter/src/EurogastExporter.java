import java.util.*;

public class EurogastExporter {
    private ArrayList<Artikel> list = new ArrayList<>();


    public EurogastExporter(ArrayList<Artikel> list) {
        this.list = list;
    }

   public static void decide(String sorten, ArrayList<Artikel> list) {
        if(sorten.equals("csv")) {
            CSV e = new CSV(list);
            e.export(list);
        }
        else {
            JSON e = new JSON(list);
            e.export(list);
        }
   }
}
