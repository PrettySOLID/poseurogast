import java.util.*;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class CSV extends Export {

    public CSV(ArrayList<Artikel> list){
        super(list);
    }

    public void export(ArrayList<Artikel> list) {
        SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println("Nummer;Bezeichnung;Einkaufspreis;Ablaufdatum;Kategorienummer");
        for(Artikel a : list){
            System.out.println(a.getNummer()+";"+ a.getBezeichnung()+";"+a.getEinkaufspreis()+";"+date.format(a.getAblaufdatum().getTime())+";"+a.getKategorienummer());
        }
    }
}
