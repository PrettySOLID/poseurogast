
import java.util.*;

public class Export {
    private ArrayList<Artikel> list = new ArrayList<>();

    public Export(ArrayList<Artikel> list) {
        this.list = list;
    }

    public void export() {
        for(Artikel a : list){
            System.out.println(a.getNummer()+" "+ a.getBezeichnung()+" "+a.getEinkaufspreis()+" "+a.getAblaufdatum()+" "+a.getKategorienummer());
        }
    }
}
