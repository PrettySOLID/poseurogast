import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class App {

	public static void main(String[] args)
	{
		//zu verarbeitende Artikelliste kommt aus untenstehender Hilfsmethode:
		ArrayList<Artikel> al = artikellisteAusDBDummy();
		
		//Je nach Kommandozeilenparameter sollen verschiedene Exporte durchgef�hrt werden.
		//In Eclipse k�nnen diese Paramter gesetzt werden �ber (noch ist keiner gesetzt):
		//�ber Run->Run Configurations -> Arguments -> Programm Arguments
		if(args.length>0)
		{
			String parameter = args[0];
			switch(parameter)
			{
			case "CSV":
				EurogastExporter.decide("csv", al);
				break;
			case "JSON":
				EurogastExporter.decide("json", al);
				break;
			default:
				EurogastExporter.decide("csv", al);
				break;
			}
		} else //kein Parameter, dann CSV-Export (Standard)
		{
			EurogastExporter.decide("csv", al);
		}
	}

	/**
	 * Diese Methode liefert eine Beispielliste mit Artikeln,
	 * wie sie von der Export-Applikation verarbeitet werden soll.
	 * Sp�ter wird diese Methode die Daten aus einer Datenbank holen.
	 * Das ist aber nicht Teil des aktuellen JAVA Export Prototpyen.
	 * @return Beispielliste, mit der gearbeitet werden soll.
	 */
	public static ArrayList<Artikel> artikellisteAusDBDummy() {
		ArrayList<Artikel> liste = new ArrayList<>();
		liste.add(new Artikel("1","Sekt",90.23,new GregorianCalendar(2019, Calendar.MARCH,4),24));
		liste.add(new Artikel("2","Fanta",9.90,new GregorianCalendar(2019,Calendar.NOVEMBER,1),23));
		liste.add(new Artikel("3","Cola",9.90,new GregorianCalendar(2019,Calendar.JANUARY,12),23));
		liste.add(new Artikel("4","Beinschinken",12.23,new GregorianCalendar(2019,Calendar.NOVEMBER,12),45));
		liste.add(new Artikel("5","Speck",5.23,new GregorianCalendar(2018,Calendar.FEBRUARY,3),45));
		return liste;
	}
}
